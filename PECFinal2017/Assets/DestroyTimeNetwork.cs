﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DestroyTimeNetwork : NetworkBehaviour
{
    public override void OnStartServer()
    {
        if(isServer)
        {
            StartCoroutine(DeleteBullet());
        }
    }

    IEnumerator DeleteBullet()
    {
        yield return new WaitForSeconds(3f);
        NetworkServer.Destroy(gameObject);
    }
}
