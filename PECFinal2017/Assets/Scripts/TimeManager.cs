﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {

    public Text time;
    float timer;

    void Start()
    {
        time.text = "00:00";
        timer = 0;
    }
    
    void Update()
    {
        timer += Time.deltaTime;

        string minutes = Mathf.Floor(timer / 60).ToString("00");
        string seconds = (timer % 60).ToString("00");

        time.text = (minutes + ":" + seconds);
    }
}
