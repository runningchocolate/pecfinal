﻿using UnityEngine;
using UnityEngine.Networking;
using System;

using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;

[Serializable]
public struct Weapon
{
    public MeshRenderer renderer;
    public int damage;
    public float range;
    public float cooldown;

    public int currentBullets;
    public int maxCurrentBullets;
    public int reloadBullets;
    public int maxBullets;

    public float reloadTime;

    public Sprite icon;

    public bool isMeele;

    internal void ChangeBullets(int v)
    {
        //Decrementacio current bullets                                                         IMPORTANT: decrementacio de bullets
        currentBullets += v;
    }

    internal void Reload()
    {
        reloadBullets = reloadBullets - (maxCurrentBullets - currentBullets);
        if (reloadBullets < 0)
        {
            currentBullets = currentBullets + reloadBullets;
            reloadBullets = 0;
        }
        else
        {
            currentBullets = maxCurrentBullets;
        }
    }

    internal void AddMunition(int m)
    {
        if (!isMeele)
        {
            reloadBullets = m;
            if (reloadBullets > maxBullets) reloadBullets = maxBullets;
        }
    }

    internal bool IsFullMunition()
    {
        return currentBullets >= maxCurrentBullets;
    }
}

public class AttackBehaviour : GenericBehaviour
{

    public string attackButton = "Fire1";
    public string changeWeaponButton = "Fire2";
    public string reloadButton = "Reload";
    public string useButton = "Use";
    public Weapon meeleWeapon;
    public Weapon rangedWeapon;
    public Vector3 halfHeight;
    public ParticleSystem shotParticle;
    public LayerMask enemyLayer;
    public AudioClip meeleAttackFx;
    public AudioClip shootAttackFx;
    public GameObject decalPrefab;

    private int enemyLayerMask;
    private int attackingBool;
    private bool attacking;
    private float zOffsetCamera;

    private int meeleBool;
    [SyncVar (hook = "OnChangedWeap")]
    private bool meele;
    private Weapon selectedWeap;
    //private GameManager gameManager;
    private AudioSource audioSource;

    private float cooldownTimer;
    private float cooldownCarTimer = 0f;
    private float reloadTimer = -1f;

    private GameObject reloadProgressBar;
    private Image reloadProgressBarImage;

    //CANVAS
    public Text ammo;
    public Image currentWeaponImage;

    private GameObject carSelected;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        attackingBool = Animator.StringToHash("Attacking");
        meeleBool = Animator.StringToHash("Meele");
        halfHeight = new Vector3(0, GetComponent<CapsuleCollider>().height / 2, 0);
    }

    public override void OnStartLocalPlayer()
    {
        ClientScene.RegisterPrefab(decalPrefab);
        ammo = GameObject.Find("WeaponAmmo").GetComponent<Text>();
        currentWeaponImage = GameObject.Find("Weapon").GetComponent<Image>();
        zOffsetCamera = behaviourManager.GetCamScript.camOffset.z*0.8f;
        CmdChangeWeapon();

        reloadProgressBar = GameObject.Find("ProgressBar");
        if (reloadProgressBar != null)
        {
            reloadProgressBar.SetActive(false);
            reloadProgressBarImage = reloadProgressBar.transform.Find("BarStatus").GetComponent<Image>();
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
    }

    void Update()
    {
        if (!isLocalPlayer)
            return;
        Debug.DrawRay(behaviourManager.GetCamScript.transform.position - behaviourManager.GetCamScript.transform.forward * zOffsetCamera, behaviourManager.GetCamScript.transform.forward * selectedWeap.range);
        cooldownTimer += Time.deltaTime;
        attacking = false;

        if (cooldownCarTimer >= 0)
        {
            cooldownCarTimer += Time.deltaTime;
            if (cooldownCarTimer > 2f)
            {
                cooldownCarTimer = -1;
            }
        }
        if (Input.GetButtonDown(changeWeaponButton))
        {
            CmdChangeWeapon();
        }
        else if (behaviourManager.GetAnim.GetBool("Aim") && Input.GetButton(attackButton))
        {
            if (cooldownTimer > selectedWeap.cooldown)
            {
                attacking = true;
                Attack();
            }
        }
        else if (Input.GetButton(useButton) && cooldownCarTimer == -1)
        {
            Debug.Log("pressed key f");
            if (carSelected != null)
            {
                cooldownCarTimer = -2;
                Debug.Log("Go car");
                carSelected.GetComponent<CarController>().enabled = true;
                carSelected.GetComponent<CarUserControl>().enabled = true;
                carSelected.transform.Find("CameraCar").GetComponent<Camera>().enabled = true;
                carSelected.transform.Find("CameraCar").GetComponent<AudioListener>().enabled = true;
                carSelected.GetComponent<CarUserInOut>().AssignPlayer(gameObject);
                gameObject.SetActive(false);
            }
        }

        //RELOAD
        if (reloadTimer != -1 && reloadTimer < selectedWeap.reloadTime)
        {
            reloadTimer += Time.deltaTime;
            reloadProgressBarImage.fillAmount = reloadTimer / selectedWeap.reloadTime;
        }
        else if (reloadTimer >= selectedWeap.reloadTime)
        {
            selectedWeap.Reload();
            ammo.text = selectedWeap.currentBullets + " / " + selectedWeap.reloadBullets;
            reloadProgressBar.SetActive(false);
            reloadTimer = -1;
        }
        else if (!selectedWeap.isMeele && Input.GetButtonDown(reloadButton))
        {
            Reload();
        }

        canSprint = !attacking;
        behaviourManager.GetAnim.SetBool(attackingBool, attacking);
    }

    public void CarDown(Transform t)
    {
        transform.position = t.position + (new Vector3(-3f, 0, 0));
        cooldownCarTimer = 0;
    }

    private void Attack()
    {
        if (cooldownTimer > selectedWeap.cooldown)
        {
            cooldownTimer = 0f;
            if (!meele)
            {
                if (selectedWeap.currentBullets == 0)
                {
                    Reload();
                }
                else
                {
                    selectedWeap.ChangeBullets(-1);
                    audioSource.clip = shootAttackFx;                                       //IMPORTANT: S'ha de descomentar, ho he fet per errors que em donava al testejar
                    if (audioSource.clip != null)
                        audioSource.Play();
                    if (shotParticle != null)
                        shotParticle.Play();

                    //Shoot                                                                   //IMPORTANT: Ho he afegit jo
                    if (isLocalPlayer)
                    {
                        ammo.text = meele
                            ? String.Empty
                            : selectedWeap.currentBullets.ToString() + " / " + selectedWeap.reloadBullets.ToString();
                    } //Falta decrementar current bullets
                }
            }
            else
            {
                audioSource.clip = meeleAttackFx;
                if (audioSource.clip != null)
                    audioSource.Play();
            }

            RaycastHit hit;
            if (Physics.Raycast(behaviourManager.GetCamScript.transform.position - behaviourManager.GetCamScript.transform.forward * zOffsetCamera, behaviourManager.GetCamScript.transform.forward, out hit, selectedWeap.range))
            {
                if (!meele && hit.transform.tag == "Pavement" || hit.transform.tag == "Structure")
                {
                    if (decalPrefab == null) return;
                    CmdSpawnDecal(hit.point, hit.normal);
                }
                else if (hit.transform.tag == "NPC")
                {
                    CmdShootNPC(hit.transform.gameObject);
                }
                else if (hit.transform.tag == "Zombie")
                {
                    CmdShootZombie(hit.transform.gameObject);
                }
            }
        }
    }

    private void Reload()
    {
        if (!selectedWeap.IsFullMunition() && reloadTimer == -1)
        {
            reloadTimer = 0;
            reloadProgressBar.SetActive(true);
        }
    }


    [Command]
    public void CmdShootZombie(GameObject zombie)
    {
        Debug.Log("Zombie hit");
        zombie.GetComponentInParent<ZombieHealth>().ZombieDamaged(selectedWeap.damage);
    }

    [Command]
    public void CmdShootNPC(GameObject NPC)
    {
        Debug.Log("NPC hit");
        NPC.GetComponentInParent<NPC>().TakeDamage(selectedWeap.damage);
    }

    [Command]
    void CmdSpawnDecal(Vector3 point, Vector3 normal)
    {
        if (!isServer) return;
        GameObject instDecal = Instantiate(decalPrefab, point + normal * 0.01f,
            Quaternion.FromToRotation(Vector3.forward, -normal));
        NetworkServer.Spawn(instDecal);
    }
    
    [Command]
    void CmdChangeWeapon()
    {
        meele = !meele;
    }

    void OnChangedWeap(bool meele)
    {
        behaviourManager.GetAnim.SetBool(meeleBool, meele);
        meeleWeapon.renderer.enabled = meele;
        rangedWeapon.renderer.enabled = !meele;
        selectedWeap = meele ? meeleWeapon : rangedWeapon;
        if (isLocalPlayer)
        {
            currentWeaponImage.sprite = selectedWeap.icon;
            ammo.text = meele ? String.Empty : selectedWeap.currentBullets.ToString() + " / " + selectedWeap.maxBullets.ToString();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Ammo"))
        {
            GetComponent<AttackBehaviour>().rangedWeapon.AddMunition(15);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!isLocalPlayer)
            return;
        Debug.Log("trigger " + other.tag);
        if (other.tag == "Car")
        {
            //if (!other.transform.GetComponent<CarUserControl>().isUsed)
            //{

                carSelected = other.transform.parent.gameObject;
            //}
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!isLocalPlayer)
            return;
        if (other.tag == "Car")
        {

           carSelected = null;
        }
    }
}
