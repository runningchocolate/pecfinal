﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Health : NetworkBehaviour {

    [HideInInspector] public int life;
    [SyncVar(hook = "OnHealthChanged")]
    [HideInInspector] public int currentLife;

    public AudioSource deathAudio;
    public AudioSource hurtAudio;
    private GameObject zombieDevorador;
    
    private Text health;
    public GameObject particleExplosion;

    public override void OnStartLocalPlayer()
    {
        health = GameObject.Find("Health").GetComponent<Text>();
        ClientScene.RegisterPrefab(particleExplosion);
    }


    void Start()
    {
        currentLife = 100;
        life = currentLife;
    }

    public void getCaught()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("OTHER: "+other.tag);
        if (other.tag == "Health")                          //NO TREURE sino no va quan agafa items
        {
            //Debug.Log("PORFI");
            if (currentLife < life)
            {
                currentLife += 10;
                if (currentLife > 100)
                {
                    currentLife = 100;
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.transform.CompareTag("Health"))
        if (collision.transform.tag == "Health")
        {
            //Debug.Log("HOLA");
            if (currentLife < life)
            {
                currentLife += 10;
                if (currentLife > 100)
                {
                    currentLife = 100;
                }
            }
        }
        else if (collision.transform.CompareTag("IACar"))
        {
            CmdCarDamage(50);
        }
    }

    public void zombieDead()
    {
        StartCoroutine(waitAfterDead());
        //currentLife -= 50;                            //Aixo fa que el player es mori...
    }

    [Command]
    public void CmdCarDamage(int damage)
    {
        if (particleExplosion != null && isServer)
        {
            GameObject ps = Instantiate(particleExplosion, transform.position, transform.rotation,null);
            NetworkServer.Spawn(ps);
        }
        currentLife -= 50;
    }


    [Command]
    public void CmdTakeDamage(GameObject zombie)
    {
        Debug.Log("TAKE DAMAGE");
        StartCoroutine(wait());
        currentLife -= 10;
        zombieDevorador = zombie;
    }

    
    public void Die()
    {
        GetComponent<Animator>().SetBool("Dead", true);
        GetComponent<MoveBehaviour>().enabled = false;
    }

    
    public void Revive()
    {
        GetComponent<Animator>().SetBool("Dead", false);
        GetComponent<MoveBehaviour>().enabled = true;
        currentLife = 100;
    }

    void OnHealthChanged(int hp)
    {
        currentLife = hp;
        if (isLocalPlayer)
        {
            if (currentLife <= 0)
            {
                if (deathAudio != null) deathAudio.Play();
                Die();
                if(zombieDevorador!=null) zombieDevorador.SendMessage("NPCDead");
            }
            else
            {
                if (hurtAudio != null) hurtAudio.PlayDelayed(0.2f);
            }
            health.text = hp.ToString();
        }

    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.75f);
        //GetComponent<Animator>().SetBool("getHitted", false);
    }

    IEnumerator waitAfterDead()
    {
        yield return new WaitForSeconds(0.5f);
        //GetComponent<Animator>().SetBool("zombieDead", true);
    }
}
