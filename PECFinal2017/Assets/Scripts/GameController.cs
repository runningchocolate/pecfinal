﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    public GameObject pause;
    public GameObject gameover;
    public GameObject win;

    [HideInInspector] public bool isPaused = false;
    [HideInInspector] public bool isGameOver = false;



    // Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape) && !isGameOver)
        {
            Pause();
        }
    }

    public void Pause()
    {
        if (Time.timeScale == 1)
        {
            isPaused = true;
            Time.timeScale = 0;
            ShowPaused();
            Cursor.lockState = CursorLockMode.None;
        }
        else if (Time.timeScale == 0)
        {
            isPaused = false;
            Time.timeScale = 1;
            HidePaused();
            Cursor.lockState = CursorLockMode.None;
        }
    }

    public void GameOver()
    {
        isPaused = true;
        isGameOver = true;
        Time.timeScale = 0;
        gameover.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    public void RestartGame()
    {
        isPaused = false;
        isGameOver = false;
        Time.timeScale = 1;
        SceneManager.LoadScene("Main");

    }

    public void Win()
    {
        isPaused = true;
        isGameOver = true;
        Time.timeScale = 0;
        win.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
    }

    public void Quit()
    {
        Application.Quit();
    }

    private void ShowPaused()
    {
        pause.SetActive(true);
    }

    private void HidePaused()
    {
        pause.SetActive(false);
    }
}
