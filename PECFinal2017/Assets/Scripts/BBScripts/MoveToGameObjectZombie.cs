﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("Navigation/MoveToGameObjectZombie")]
    [Help("Moves the game object towards a given target by using a NavMeshAgent")]
    public class MoveToGameObjectZombie : GOAction
    {
        [InParam("currentTarget")]
        [Help("Target game object towards this game object will be moved")]
        public GameObject currentTarget;

        private UnityEngine.AI.NavMeshAgent navAgent;

        private Transform targetTransform;

        public override void OnStart()
        {
            if (currentTarget == null)
            {
                return;
            }
            gameObject.GetComponent<ZombieHealth>().target = currentTarget;           

            if (currentTarget.tag == "NPC")
            {
                if (currentTarget.GetComponent<NPC>().life <= 0)
                {
                    gameObject.GetComponent<Animator>().SetBool("targetDeath", true);
                }
                else
                {
                    gameObject.GetComponent<Animator>().SetBool("targetDeath", false);
                }
            }
            else if (currentTarget.tag == "Player")
            {
                if (currentTarget.GetComponent<Health>().currentLife <= 0)
                {
                    gameObject.GetComponent<Animator>().SetBool("targetDeath", true);
                }
                else {
                    gameObject.GetComponent<Animator>().SetBool("targetDeath", false);
                }
            }

            targetTransform = currentTarget.transform;
			gameObject.transform.LookAt(targetTransform);

            navAgent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
            if (navAgent == null)
            {
                Debug.LogWarning("The " + gameObject.name + " game object does not have a Nav Mesh Agent component to navigate. One with default values has been added", gameObject);
                navAgent = gameObject.AddComponent<UnityEngine.AI.NavMeshAgent>();
            }

            gameObject.GetComponent<Animator>().SetBool("targetInRange", false);

            navAgent.SetDestination(targetTransform.position);
            gameObject.GetComponent<Animator>().SetFloat("speed", 1f);
            navAgent.speed = 1f;
            
            #if UNITY_5_6_OR_NEWER
                navAgent.isStopped = false;
            #else
                navAgent.Resume();
            #endif
        }

        public override TaskStatus OnUpdate()
        {
            if (currentTarget == null)
                return TaskStatus.FAILED;
			if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance) {
				navAgent.velocity = Vector3.zero;
				navAgent.isStopped = true;
				return TaskStatus.COMPLETED;
			}
            else if (navAgent.destination != targetTransform.position)
                navAgent.SetDestination(targetTransform.position);
            return TaskStatus.RUNNING;
        }

        public override void OnAbort()
        {

        #if UNITY_5_6_OR_NEWER
            if(navAgent!=null)
                navAgent.isStopped = true;
        #else
            if (navAgent!=null)
                navAgent.Stop();
        #endif

        }
    }
}
