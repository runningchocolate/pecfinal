﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using UnityEngine.AI;

namespace BBUnity.Actions
{

    [Action("Navigation/MoveToRandomPositionNPC")]
    [Help("Gets a random position from a given area and moves the game object to that point by using a NavMeshAgent")]
    public class MoveToRandomPositionNPC : GOAction
    {
        private UnityEngine.AI.NavMeshAgent navAgent;

        private int nextWayPoint = 0;

        [InParam("area")]
        [Help("game object that must have a BoxCollider or SphereColider, which will determine the area from which the position is extracted")]
        public GameObject area;

        public override void OnStart()
        {
            navAgent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
            gameObject.GetComponent<Animator>().SetBool("zombieDead", false);
			
            if (navAgent == null)
            {
                Debug.LogWarning("The " + gameObject.name + " game object does not have a Nav Mesh Agent component to navigate. One with default values has been added", gameObject);
                navAgent = gameObject.AddComponent<UnityEngine.AI.NavMeshAgent>();
            }

            //navAgent.areaMask = 17;                                                             //NavMeshAgent per zones Walkable i Crossroad
            navAgent.SetAreaCost(8, 10f);
            NavMeshHit hit;
            navAgent.SamplePathPosition(-1, 0.0f, out hit);
            if (hit.mask == 8)
            {
                navAgent.SetDestination(closestArea());
            }
            else
            {
                navAgent.areaMask = 17;
                navAgent.SetDestination(getRandomPosition());
            }
            
            gameObject.GetComponent<Animator>().SetFloat("speed", 0.4f);

#if UNITY_5_6_OR_NEWER
            navAgent.isStopped = false;
            #else
                navAgent.Resume();
            #endif
        }

        public override TaskStatus OnUpdate()
        {
            if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance)
                return TaskStatus.COMPLETED;
            return TaskStatus.RUNNING;
        }

        private Vector3 closestArea()
        {
            GameObject foundGameObject = null;

            float dist = float.MaxValue;
            foreach (GameObject go in GameObject.FindGameObjectsWithTag("Pavement"))
            {
                float newdist = (go.transform.position - gameObject.transform.position).sqrMagnitude;
                if (newdist < dist)
                {
                    dist = newdist;
                    foundGameObject = go;
                }
            }

            MeshCollider collider = foundGameObject.GetComponent<MeshCollider>();
            Vector3 returned;
            if (collider == null)
            {
                returned = foundGameObject.gameObject.GetComponent<BoxCollider>().ClosestPoint(gameObject.transform.position);
            }
            else
            {
                returned = collider.ClosestPoint(gameObject.transform.position);
            }

            if (returned[0] > 0)
            {
                returned[0] += 2;
            }
            else if (returned[0] < 0)
            {
                returned[0] -= 2;
            }

            if (returned[2] > 0)
            {
                returned[2] += 2;
            }
            else if (returned[2] < 0)
            {
                returned[2] -= 2;
            }

            return returned;
        }

        private Vector3 getRandomPosition()
        {
            //navAgent.areaMask = 17;
            BoxCollider boxCollider = area != null ? area.GetComponent<BoxCollider>() : null;
            if (boxCollider != null)
            {
                return new Vector3(UnityEngine.Random.Range(area.transform.position.x - area.transform.localScale.x * boxCollider.size.x * 1f,
                                                            area.transform.position.x + area.transform.localScale.x * boxCollider.size.x * 1f),
                                   area.transform.position.y,
                                   UnityEngine.Random.Range(area.transform.position.z - area.transform.localScale.z * boxCollider.size.z * 1f,
                                                            area.transform.position.z + area.transform.localScale.z * boxCollider.size.z * 1f));
            }
            else
            {
                SphereCollider sphereCollider = area != null ? area.GetComponent<SphereCollider>() : null;
                if (sphereCollider != null)
                {
                    float distance = UnityEngine.Random.Range(-sphereCollider.radius, area.transform.localScale.x * sphereCollider.radius);
                    float angle = UnityEngine.Random.Range(0, 2 * Mathf.PI);
                    return new Vector3(area.transform.position.x + distance * Mathf.Cos(angle),
                                       area.transform.position.y,
                                       area.transform.position.z + distance * Mathf.Sin(angle));
                }
                else
                {
                    return gameObject.transform.position + new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5f, 5f));
                }
            }
        }

        public override void OnAbort()
        {
            #if UNITY_5_6_OR_NEWER
                navAgent.isStopped = true;
            #else
                navAgent.Stop();
            #endif
        }
    }
}
