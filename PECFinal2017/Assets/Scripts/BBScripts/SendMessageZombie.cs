﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BBUnity.Actions
{

    [Action("GameObject/SendMessageZombie")]
    [Help("Calls the method named methodName on every MonoBehaviour in this game object")]
    public class SendMessageZombie : GOAction
    {
        [InParam("methodName")]
        [Help("Name of the method that must be called")]
        public string methodName;

        [InParam("currentTarget")]
        [Help("Target")]
        public GameObject targetGameobject;

        public override void OnStart()
        {
            if (targetGameobject != null)
            {
                //Debug.Log("TARGET: " + targetGameobject);
                if (targetGameobject.tag == "NPC")
                {
                    //targetGameobject.SendMessage(methodName);           //Not used
                    if (targetGameobject.GetComponent<NPC>().life > 0)          //Faig aixo perque sino en una repeticio residual, encara que hagi mort el npc o jugador, pegaria a l'aire
                    {
                        gameObject.GetComponent<Animator>().SetBool("targetInRange", true);
                    }
                }
                else if (targetGameobject.tag == "Player")
                {
                    if (targetGameobject.GetComponent<Health>().currentLife > 0)          //Faig aixo perque sino en una repeticio residual, encara que hagi mort el npc o jugador, pegaria a l'aire
                    {
                        gameObject.GetComponent<Animator>().SetBool("targetInRange", true);
                    }
                }
            }
        }

        public override TaskStatus OnUpdate()
        {
            return TaskStatus.COMPLETED;
        }
    }
}
