﻿using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{
    [Condition("Perception/IsTargetCloseNPC")]
    [Help("Checks whether a target is close depending on a given distance")]
    public class IsTargetCloseNPC : GOCondition
    {
        [InParam("zombieTag")]
        [Help("TagTarget to check the distance")]
        public string tagTarget;

        [OutParam("currentTarget")]
        [Help("Target to check the distance")]
        public GameObject currentTarget;

        [InParam("closeDistance")]
        [Help("The maximun distance to consider that the target is close")]
        public float closeDistance;

        private float lastDistance;
        private bool found;
        private float rateRefresh = 1.0f;
        private float timerRefresh = 1.1f;
        GameObject[] targets;

        public override bool Check()
		{
            timerRefresh += Time.deltaTime;
            if (rateRefresh < timerRefresh)
            {
                targets = GameObject.FindGameObjectsWithTag(tagTarget);
                timerRefresh -= rateRefresh;
            }

            lastDistance = 1000;
            found = false;

            if (targets.Length > 0) {
                for(int i = 0; i < targets.Length; i++)
                {
                    if (targets[i] == null) continue;
                    float distanceActualTarget = (gameObject.transform.position - targets[i].transform.position).sqrMagnitude;
                    if (distanceActualTarget<lastDistance && distanceActualTarget < closeDistance * closeDistance)
                    {
                        currentTarget = targets[i];
                        lastDistance = distanceActualTarget;
                        found = true;
                    }
                }
                return found;
            }
            else
            {
                gameObject.GetComponent<Animator>().SetBool("zombieDead", false);
                return false;
            }				
		}
    }
}