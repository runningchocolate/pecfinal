﻿using Pada1.BBCore.Tasks;
using Pada1.BBCore;
using UnityEngine;
using UnityEngine.AI;

namespace BBUnity.Actions
{

    [Action("Navigation/MoveToPositionNPC")]
    [Help("Moves the game object to a given position by using a NavMeshAgent")]
    public class MoveToPositionNPC : GOAction
    {
        [InParam("currentTarget")]
        [Help("Target position where the game object will be moved")]
        public GameObject target;

        private NavMeshAgent navAgent;
        private Vector3 targetTransform;

        AudioSource[] fleeAudio;

        public override void OnStart()
        {

            if (target == null)
            {
                return;
            }

            AudioSource finalAudio = null;
            fleeAudio = gameObject.GetComponents<AudioSource>();

            foreach (AudioSource flee in fleeAudio) {
                if (flee.clip.name.Contains("flee")){
                    finalAudio = flee;
                }
            }

            navAgent = gameObject.GetComponent<NavMeshAgent>();
            if (navAgent == null)
            {
                Debug.LogWarning("The " + gameObject.name + " game object does not have a Nav Mesh Agent component to navigate. One with default values has been added", gameObject);
                navAgent = gameObject.AddComponent<NavMeshAgent>();
            }

            if (target.GetComponent<ZombieHealth>().life > 0)
            {
                navAgent.areaMask = 25;                                                             //NavMeshAgent per zones Walkable i Crossroad i Road
                navAgent.SetAreaCost(8, 1f);

                Vector3 moveDir = gameObject.transform.position - target.transform.position;
                targetTransform = gameObject.transform.position + moveDir.normalized * 2;
                navAgent.SetDestination(targetTransform);
                gameObject.GetComponent<Animator>().SetFloat("speed", 1f);

                if(finalAudio!=null)finalAudio.Play();
            }
            else
            {
                OnUpdate();
            }

            #if UNITY_5_6_OR_NEWER
                navAgent.isStopped = false;
            #else
                navAgent.Resume();
            #endif
        }

        public override TaskStatus OnUpdate()
        {
            if (target == null)
                return TaskStatus.FAILED;
            if (!navAgent.pathPending && navAgent.remainingDistance <= navAgent.stoppingDistance || target.GetComponent<ZombieHealth>().life <= 0)
            {
                navAgent.velocity = Vector3.zero;
                navAgent.isStopped = true;
                return TaskStatus.COMPLETED;
            }
            else if (navAgent.destination != targetTransform)
                navAgent.SetDestination(targetTransform);

            return TaskStatus.RUNNING;
        }

        public override void OnAbort()
        {
#if UNITY_5_6_OR_NEWER
            if(navAgent!=null)
                navAgent.isStopped = true;
#else
            if (navAgent != null)
                navAgent.Stop();
            #endif
        }
    }
}
