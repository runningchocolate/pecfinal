﻿using Pada1.BBCore;
using UnityEngine;

namespace BBUnity.Conditions
{
    [Condition("Perception/IsTargetCloseZombie")]
    [Help("Checks whether a target is close depending on a given distance")]
    public class IsTargetCloseZombie : GOCondition
    {
        [InParam("targetNpc")]
        [Help("TagTarget to check the distance")]
        public string tagNPC;

        [InParam("targetPlayer")]
        [Help("TagTarget to check the distance")]
        public string tagPlayer;

        [OutParam("currentTarget")]
        [Help("Target to check the distance")]
        public GameObject currentTarget;

        [InParam("closeDistance")]
        [Help("The maximun distance to consider that the target is close")]
        public float closeDistance;

        private float lastDistance;
        private bool found;
        private float rateRefresh = 1.0f;
        private float timerRefresh = 1.1f;
        private GameObject[] npcs;
        private GameObject[] players;

        public override bool Check()
		{
            timerRefresh += Time.deltaTime;
            if (rateRefresh < timerRefresh)
            {
                npcs = GameObject.FindGameObjectsWithTag(tagNPC);
                players = GameObject.FindGameObjectsWithTag(tagPlayer);
                timerRefresh -= rateRefresh;
            }

            found =false;
            lastDistance = 1000;
            if (npcs.Length > 0)
            {
                for (int i = 0; i < npcs.Length; i++)
                {
                    if (npcs[i] == null) continue;
                    float distanceActualTarget = (gameObject.transform.position - npcs[i].transform.position).sqrMagnitude;
                    if (distanceActualTarget < lastDistance && distanceActualTarget < closeDistance * closeDistance)
                    {
                        currentTarget = npcs[i];
                        lastDistance = distanceActualTarget;
                        found = true;
                    }
                }
            }
        
            if (players.Length > 0)
            {
                for (int i = 0; i < players.Length; i++)
                {
                    if (players[i] == null) continue;
                    float distanceActualTarget = (gameObject.transform.position - players[i].transform.position).sqrMagnitude;
                    if (distanceActualTarget < lastDistance && distanceActualTarget < closeDistance * closeDistance)
                    {
                        currentTarget = players[i];
                        lastDistance = distanceActualTarget;
                        found = true;
                    }
                }
            }
            /*
            if (currentTarget.CompareTag(tagPlayer))
            {
                    
            }
            else if(currentTarget.CompareTag(tagNPC))
            {

            }
            */
            return found;
		}
    }
}

