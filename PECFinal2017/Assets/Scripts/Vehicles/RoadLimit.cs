﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Vehicles.Car;

public class RoadLimit : NetworkBehaviour {
    [HideInInspector]
    public enum RoadLimitType
    {
        SpeedLimit,
        RoadEnd
    }

    public RoadLimitType roadLimitTypeVar = RoadLimitType.RoadEnd;

    private void OnTriggerEnter(Collider other)
    {
        if (!isServer) return;
        if (roadLimitTypeVar == RoadLimitType.RoadEnd)
        {
            if (other.gameObject.tag == "IACar")
            {
                NetworkServer.Destroy(other.gameObject);
            }            
        }
        else if (roadLimitTypeVar == RoadLimitType.SpeedLimit)
        {
            if (other.gameObject.tag == "IACar")
            {
                other.gameObject.GetComponent<CarChildController>().ChangeMaxSpeed();
            }
        }
    }
}
