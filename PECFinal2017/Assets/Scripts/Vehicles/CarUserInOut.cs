﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class CarUserInOut : MonoBehaviour {

    public string useButton = "Use";
    [HideInInspector] public GameObject player = null;

    public int secondsToDown = 1;
    private float secondsToDownCount = -1;

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        if(secondsToDownCount!=-1) secondsToDownCount += Time.deltaTime;
        if (player != null && secondsToDownCount > secondsToDown)
        {
            if (Input.GetButton(useButton))
            {
                secondsToDownCount = -1;
                GetComponent<CarController>().enabled = false;
                GetComponent<CarUserControl>().enabled = false;
                transform.Find("CameraCar").GetComponent<Camera>().enabled = false;
                transform.Find("CameraCar").GetComponent<AudioListener>().enabled = false;
                player.SetActive(true);
                player.GetComponent<AttackBehaviour>().CarDown(transform);
                player = null;

            }
        }
    }

    public void AssignPlayer(GameObject p)
    {
        player = p;
        secondsToDownCount = 0f;
    }
}
