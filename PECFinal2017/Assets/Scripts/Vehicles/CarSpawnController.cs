﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Utility;



public class CarSpawnController : NetworkBehaviour {

    List<GameObject> entities;
    List<Circuit> circuits;

    public float SpawnFrequency = 7f;
    public int Spawns = -1;
    int spawnCount = 0;

    public GameObject[] cars;
    public GameObject[] circuitsGO;
    public Transform[] spawnPoints;

    public override void OnStartClient()
    {
        foreach (GameObject obj in cars)
        {
            ClientScene.RegisterPrefab(obj);
        }
        base.OnStartClient();
    }

    public override void OnStartServer()
    {
        
        if (!isServer) return;
        entities = new List<GameObject>();
        InvokeRepeating("Spawn", 0f, SpawnFrequency);
        circuits = new List<Circuit>();
        for (int i = 0; i < circuitsGO.Length; i++)
        {
            circuits.Add(new Circuit
            {
                startPoint = spawnPoints[i],
                circuit = circuitsGO[i].GetComponent<WaypointCircuit>()
                
            });
        }
    }

    void Spawn()
    {
        if ((Spawns == -1 || spawnCount <= Spawns) && cars.Length > 0)
        {
            int cInt = Random.Range(0, cars.Length);
            GameObject prefab = cars[cInt];

            int circInt = Random.Range(0, circuits.Count);
            Circuit c = circuits[circInt];

            GameObject instance = Instantiate(prefab, c.startPoint.position, Quaternion.Euler(new Vector3(0, c.startPoint.eulerAngles.y, 0)));

            instance.GetComponent<WaypointProgressTracker>().circuit = c.circuit;
            instance.GetComponent<WaypointProgressTracker>().Reset();
            NetworkServer.Spawn(instance);

            ++spawnCount;

            entities.Add(instance);
        }
        else
        {
            CancelInvoke();
        }

    }
}
