﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class CarChildController : CarController
{

    public float intoCitySpeed = 50f;
    public float outsideCitySpeed = 100f;

    public enum CarUbication
    {
        outsideCity,
        insideCity
    }

    public CarUbication actualUbication = CarUbication.outsideCity;

    public void ChangeMaxSpeed()
    {
        if (CarUbication.outsideCity == actualUbication)
        {
            actualUbication = CarUbication.insideCity;
            m_Topspeed = intoCitySpeed;            
        }
        else if (CarUbication.insideCity == actualUbication)
        {
            actualUbication = CarUbication.outsideCity;
            m_Topspeed = outsideCitySpeed;
        }
    }

    
}
