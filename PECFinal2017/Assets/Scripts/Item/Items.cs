﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour {

    float rotationTime;
    AudioSource itemAudio;

    // Use this for initialization
    void Start()
    {
        rotationTime = 2f;
        itemAudio = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(0, 360 * Time.deltaTime / rotationTime, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            itemAudio.Play();
            Destroy(this.gameObject, 0.6f);
        }
    }
}
