﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomItemsInArea : MonoBehaviour {

    float rotationTime;
    public Transform gameAreaCenter;
    public float gameAreaSizeX;
    public float gameAreaSizeZ;

    AudioSource gameAudio;

	// Use this for initialization
	void Start () {
        rotationTime = 2f;
        Respawn();
        gameAudio = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Rotate(0, 360 * Time.deltaTime / rotationTime, 0);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            gameAudio.Play();
            Respawn();
        }
    }

    void Respawn()
    {
        this.transform.position = new Vector3(gameAreaCenter.position.x + Random.Range(-gameAreaSizeX / 3.5f, gameAreaSizeX / 3.5f), 50, gameAreaCenter.position.z + Random.Range(-gameAreaSizeZ / 3.5f, gameAreaSizeZ / 3.5f));
        RaycastHit hit;
        if (Physics.Raycast(new Ray(this.transform.position, Vector3.down), out hit, 100f))
        {
            this.transform.position = hit.point + new Vector3(0, 1f, 0);
        }
    }
}
