﻿using UnityEngine;

namespace CompleteProject
{
    public class NPCManager : MonoBehaviour
    {
        //public PlayerLife playerHealth;       
        public GameObject NPC;              
        public float spawnTime = 20f;          
        public Transform[] spawnPoints;

        int enemies;

        float timer;



        

        void Start ()
        {
            

            InvokeRepeating ("Spawn", 0, spawnTime);
            timer = 0f;

        }

        private void Update()
        {
            /*enemies = GameObject.FindGameObjectsWithTag("EnemyCounter").Length;

            if (enemies > 150)
            {
                CancelInvoke("Spawn");
            }

            timer += Time.deltaTime;

            if (timer > 10f)
            {
                if (spawnTime > 1 && enemies < 150)
                {
                    spawnTime -= 0.5f;
                    CancelInvoke("Spawn");
                    InvokeRepeating("Spawn", 0, spawnTime);
                }
                timer = 0f;
            }*/
        }

        void Spawn ()
        {
            /*if(playerHealth.currentHealth <= 0f)
            {
                return;
            } */           
            int spawnPointIndex = Random.Range (0, spawnPoints.Length);          
            Instantiate (NPC, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);
        }
    }
}