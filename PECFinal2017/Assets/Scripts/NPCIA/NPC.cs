﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class NPC : NetworkBehaviour
{
    [SyncVar (hook=("OnLifeChanged"))]
    [HideInInspector] public int life;
    [HideInInspector] public GameObject zombie;
    public Material transparent;
    float timer = 0f;
    private NavMeshAgent navAgent;

    public AudioSource deadAudio;
    public AudioSource getHurt;

    public GameObject particleExplosion;

    //public Transform[] waypoints;

    public GameObject heads;
    public GameObject bodies;

    [SyncVar]
    private bool lifeInitialized = false;

    [SyncVar (hook="OnBodySelected")]
    private int bodySelected;
    [SyncVar(hook = "OnHeadSelected")]
    private int headSelected;

    public override void OnStartClient()
    {
        if (isServer)
        {
            Transform[] bodies_transform = this.bodies.transform.GetComponentsInChildren<Transform>();
            bodySelected = UnityEngine.Random.Range(0, bodies_transform.Length - 1);

            Transform[] heads_transform = this.heads.transform.GetComponentsInChildren<Transform>();
            headSelected = UnityEngine.Random.Range(0, heads_transform.Length - 1);
        }
    }

    void OnBodySelected(int nbody)
    {
        bodySelected = nbody;
        Transform[] bodies_transform = this.bodies.transform.GetComponentsInChildren<Transform>();
        int i = 0;
        foreach (Transform t in bodies_transform)
        {
            if (t != bodies.transform)
            {
                if (bodySelected != i) t.gameObject.SetActive(false);
            }
            else if (bodySelected == i) bodySelected++;
            i++;
        }
    }

    void OnHeadSelected(int nHead)
    {
        headSelected = nHead;
        Transform[] heads_transform = this.heads.transform.GetComponentsInChildren<Transform>();
        int i = 0;
        foreach (Transform t in heads_transform)
        {
            if (t != heads.transform)
            {
                if (headSelected != i) t.gameObject.SetActive(false);
            }
            else if (headSelected == i) headSelected++;
            i++;
        }
    }

    void Start()
    {
        navAgent = gameObject.GetComponent<NavMeshAgent>();
        life = 40;
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Zombie")
        {
            zombie = other.gameObject;
        }
        else if (other.tag == "IACar" || other.tag == "Car")
        {
            if (particleExplosion != null)
            {
                GameObject ps = Instantiate(particleExplosion, transform.position, transform.rotation,null);
                NetworkServer.Spawn(ps);
            }
            NetworkServer.Destroy(this.gameObject);
            getHurt.Play();
        }
    }

    public void zombieDead()
    {
        StartCoroutine(waitAfterDead());

    }

    public void TakeDamage(int dmg)
    {
        life -= dmg;
    }

    public void OnLifeChanged(int newHP)
    {
        life = newHP;
        if (!lifeInitialized)
        {
            lifeInitialized = true;
            return;
        }
        if (life > 0)
            if (getHurt != null) getHurt.Play();

        if (!isServer) return;
        if (navAgent!=null && navAgent.enabled)
        {
            navAgent.velocity = Vector3.zero;
            navAgent.isStopped = true;
        }
        GetComponent<Animator>().SetBool("getHitted", true);
        StartCoroutine(wait());
        if (life <= 0)
        {
            if (zombie != null) zombie.SendMessage("NPCDead");
            GetComponent<Animator>().SetTrigger("isDead");
            StartCoroutine(dead());
        }
        else
        {
            if (getHurt != null) getHurt.Play();
        }
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.75f);
        GetComponent<Animator>().SetBool("getHitted", false);
    }

    IEnumerator waitAfterDead()
    {
        yield return new WaitForSeconds(0.5f);
        GetComponent<Animator>().SetBool("zombieDead", true);
    }

    IEnumerator dead()
    {
        if(deadAudio!=null)deadAudio.Play();
        yield return new WaitForSeconds(5f);
        float counter = 0f;

        SkinnedMeshRenderer[] array = GetComponentsInChildren<SkinnedMeshRenderer>();
        MeshRenderer[] arrayMesh = GetComponentsInChildren<MeshRenderer>();

        foreach (SkinnedMeshRenderer item in array)
        {
            item.material = transparent;
        }
        foreach (MeshRenderer itemMesh in arrayMesh)
        {
            itemMesh.material = transparent;
        }

        while (counter < 2)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(1, 0, counter / 2);
            foreach (SkinnedMeshRenderer item in array)
            {
                item.material.color = new Color(item.material.color.r, item.material.color.g, item.material.color.b, alpha);
            }
            foreach (MeshRenderer itemMesh in arrayMesh)
            {
                itemMesh.material.color = new Color(itemMesh.material.color.r, itemMesh.material.color.g, itemMesh.material.color.b, alpha);
            }
            yield return null;
        }
        NetworkServer.Destroy(this.gameObject);
    }
}
