﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieHit : MonoBehaviour {

    bool attacking;
    public int zombieDamage = 10;
    public AudioSource attackAudio;
    public GameObject zombie;

    private void Start()
    {
        attacking = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("NPC"))
        {
            if (other.gameObject.GetComponent<NPC>().life > 0 & attacking)
            {
                if (attackAudio != null) attackAudio.Play();
                other.GetComponent<NPC>().TakeDamage(zombieDamage);
                attacking = false;
                StartCoroutine(wait());
            }            
        }
        else if (other.gameObject.CompareTag("Player"))
        {
            if (other.gameObject.GetComponent<Health>().currentLife > 0 & attacking)
            {
                if(attackAudio!=null) attackAudio.Play();
                other.GetComponent<Health>().CmdTakeDamage(zombie);
                attacking = false;
                StartCoroutine(wait());
            }
        }
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(1.3f);
        attacking = true;
    }

}
