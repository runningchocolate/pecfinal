﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

public class ZombieHealth : NetworkBehaviour
{

    [SyncVar(hook = ("OnLifeChanged"))]
    [HideInInspector] public int life;
    [HideInInspector] public GameObject target;
    [HideInInspector] NavMeshAgent navAgent;
    public Material transparent;

    public GameObject health;
    public GameObject bigHealth;
    public GameObject ammo;

    public AudioSource getHurt;
    public AudioSource deadSound;

    public GameObject particleExplosion;

    public GameObject heads;
    public GameObject bodies;

    float timer = 0f;
    private bool lifeInitialized = false;

    void Start()
    {
        life = 50;
        navAgent = GetComponent<NavMeshAgent>();

        Transform[] bodies_transform = this.bodies.transform.GetComponentsInChildren<Transform>();
        int r = UnityEngine.Random.Range(0, bodies_transform.Length - 1);
        int i = 0;
        foreach (Transform t in bodies_transform)
        {
            if (t != bodies.transform)
            {
                if (r != i) t.gameObject.SetActive(false);
            }
            else if (r == i) r++;
            i++;
        }

        Transform[] heads_transform = this.heads.transform.GetComponentsInChildren<Transform>();
        r = UnityEngine.Random.Range(0, heads_transform.Length - 1);
        i = 0;
        foreach (Transform t in heads_transform)
        {
            if (t != heads.transform)
            {
                if (r != i) t.gameObject.SetActive(false);
            }
            else if (r == i) r++;
            i++;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("NPC") || other.CompareTag("Player"))
        {
            target = other.gameObject;
        }
        else if (other.tag == "IACar" || other.tag == "Car")
        {
            if (particleExplosion != null)
            {
                GameObject ps = Instantiate(particleExplosion, transform.position, transform.rotation,null);
                NetworkServer.Spawn(ps);
            }
            NetworkServer.Destroy(this.gameObject);
            getHurt.Play();
        }
    }

    public void ZombieDamaged(int damage)
    {
        CmdZombieIsAttacked(damage);
    }

    [Command]
    private void CmdZombieIsAttacked(int damage)
    {
        
        life -= damage;
        
    }

    public void OnLifeChanged(int newHP)
    {
        life = newHP;
        if (!lifeInitialized)
        {
            lifeInitialized = true;
            return;
        }
        GetComponent<Animator>().SetBool("getHitted", true);
        StartCoroutine(wait());

        if (life <= 0)
        {
            target.SendMessage("zombieDead");
            StartCoroutine(waitDead());
            GetComponent<Animator>().SetTrigger("isDead");
            navAgent.velocity = Vector3.zero;
            navAgent.isStopped = true;
            StartCoroutine(dead());
        }
        else
        {
            getHurt.Play();
        }
    }

    public void NPCDead()
    {
        GetComponent<Animator>().SetBool("targetDeath", true);
    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(0.8f);
        GetComponent<Animator>().SetBool("getHitted", false);
    }

    IEnumerator waitDead()
    {
        yield return new WaitForSeconds(0.2f);
        GetComponent<BehaviorExecutor>().enabled = false;
        GetComponentInChildren<ZombieHit>().enabled = false;
        deadSound.Play();
    }

    IEnumerator dead()
    {
        yield return new WaitForSeconds(5f);
        float counter = 0f;

        SkinnedMeshRenderer[] array = GetComponentsInChildren<SkinnedMeshRenderer>();
        MeshRenderer[] arrayMesh = GetComponentsInChildren<MeshRenderer>();

        foreach (SkinnedMeshRenderer item in array)
        {
            item.material = transparent;
        }
        foreach (MeshRenderer itemMesh in arrayMesh)
        {
            itemMesh.material = transparent;
        }

        while (counter < 2)
        {
            counter += Time.deltaTime;
            float alpha = Mathf.Lerp(1, 0, counter / 2);
            foreach (SkinnedMeshRenderer item in array)
            {
                item.material.color = new Color(item.material.color.r, item.material.color.g, item.material.color.b, alpha);
            }
            foreach (MeshRenderer itemMesh in arrayMesh)
            {
                itemMesh.material.color = new Color(itemMesh.material.color.r, itemMesh.material.color.g, itemMesh.material.color.b, alpha);
            }
            yield return null;
        }
        NetworkServer.Destroy(this.gameObject);
        setItem();
        //Instantiate(item, transform.position + new Vector3(0, 1f, 0f), transform.rotation);
    }

    public void setItem()
    {
        int a = Random.Range(1, 3);
        GameObject h;
        switch (a)
        {
            case 1:
                h = Instantiate(health, transform.position + new Vector3(0, 1f, 0), Quaternion.identity);
                h.tag = "Health";
                break;
            case 2:
                h = Instantiate(bigHealth, transform.position + new Vector3(0, 1f, 0), Quaternion.identity);
                h.tag = "Health";
                break;
            case 3:
                h = Instantiate(ammo, transform.position + new Vector3(0, 1f, 0), Quaternion.identity);
                h.tag = "Ammo";
                break;
        }
    }
}
