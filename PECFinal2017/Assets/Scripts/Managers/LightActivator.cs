﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightActivator : MonoBehaviour
{
    private Light[] myLights;

    public float factorMinutesLeftToDesactivate = 0.2f;

    public float factorMinutesLeftToActivate = 0.1f;

    private DayNightManager dayNight;
    private MeshRenderer myRenderer;

    private bool turned = true;

	// Use this for initialization
	void Start ()
	{
	    dayNight = GameObject.Find("DayNightManager").GetComponent<DayNightManager>();
	    myLights = GetComponentsInChildren<Light>();
	    turned = true;
        Turn();
	}
	
	// Update is called once per frame
	void Update () {
	    if (dayNight.secondsLeft!=-1 && dayNight.secondsLeft <= dayNight.totalSecondsTo12hours * factorMinutesLeftToActivate && !turned)
	    {
            //openLights
	        Turn();
	    }
	    else if(dayNight.secondsPassed != -1 && turned 
	                                         && dayNight.secondsPassed >= dayNight.totalSecondsTo12hours * factorMinutesLeftToDesactivate 
	                                         && dayNight.secondsLeft >= dayNight.totalSecondsTo12hours * factorMinutesLeftToActivate)
	    {
            //closeLights
	        Turn();
	    }
	}

    void Turn()
    {
        turned = !turned;
        foreach (Light l in myLights)
        {
            l.enabled = turned;
        }
    }
}
