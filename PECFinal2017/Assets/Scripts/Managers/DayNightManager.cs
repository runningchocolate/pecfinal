﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DayNightManager : NetworkBehaviour
{

    public GameObject directional;
    public float totalSecondsTo12hours = 30;
    public float startDayRotationX = 0;
    public float endDayRotationX = 180;

    public float secondsLeft = -1;
    public float secondsPassed = -1;
    public bool isNight;
    public int daysPassed = 0;

    private float elapsedTime;
    private float totalSecondsDay;


    // Use this for initialization
    void Start ()
	{
	    Reset();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    elapsedTime += Time.deltaTime;
	    float rot = (endDayRotationX - startDayRotationX) / totalSecondsTo12hours;
        directional.transform.Rotate(rot*Time.deltaTime,0, 0);

	    secondsLeft = totalSecondsTo12hours - (elapsedTime - daysPassed*totalSecondsDay);
	    secondsPassed = elapsedTime - daysPassed * totalSecondsDay;

	    if (totalSecondsTo12hours >= 90) isNight = true;

	    if (Input.GetKeyDown(KeyCode.R)) Reset();

        
        //Debug.Log(secondsLeft + " " + secondsPassed + " " + totalSecondsDay);
	    if (totalSecondsDay <= secondsPassed) daysPassed++;
	}

    void Reset()
    {
        daysPassed = 0;
        isNight = false;
        secondsLeft = -1;
        secondsPassed = -1;
        totalSecondsDay = 24 * totalSecondsTo12hours / 12;
        totalSecondsTo12hours--;
        elapsedTime = 0;
        directional.transform.rotation = Quaternion.Euler(startDayRotationX, directional.transform.rotation.eulerAngles.y, directional.transform.rotation.eulerAngles.z);
    }
}
