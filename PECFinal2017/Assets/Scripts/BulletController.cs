﻿using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float bulletDamage = 10f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Zombie")
        {
            //col.gameObject.GetComponent<CharacterZombie>().Hit(bulletDamage);
            Destroy(gameObject);
        }
        else if (col.gameObject.tag == "Player")
        {
            //col.gameObject.GetComponent<PlayerController>().Hit(bulletDamage);
            Destroy(gameObject);
        }
    }
}
