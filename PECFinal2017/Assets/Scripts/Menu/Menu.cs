﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void NewGame()
    {
        SceneManager.LoadScene("NetworkLobby");
    }

    public void Options()
    {
        SceneManager.LoadScene("OptionsMenu");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
