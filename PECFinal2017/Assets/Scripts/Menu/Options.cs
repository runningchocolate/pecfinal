﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Options : MonoBehaviour {

    public Slider musicSlider;
    public Slider seSlider;
    public AudioMixer masterMixer;

    // Use this for initialization
    void Start () {

        float value;
        masterMixer.GetFloat("musicVol", out value);
        musicSlider.value = value;

        Debug.Log("MUSIC1: " + value);

        masterMixer.GetFloat("sfxVol", out value);
        seSlider.value = value;
    }

    public void SetSfxLvl(float sfxLvl)
    {
        masterMixer.SetFloat("sfxVol", sfxLvl);
    }

    public void SetMusicLvl(float musicLvl)
    {
        masterMixer.SetFloat("musicVol", musicLvl);
        Debug.Log("MUSIC: " + musicLvl);
    }

    public void Back()
    {
        SceneManager.LoadScene("Menu");
    }
}
