﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NPCSpawner : NetworkBehaviour
{

    public GameObject[] npcPrefabs;
    public int maxNpcs;
    public float rateSecondsSpawn;
    public int initialNpcs = 10;

    private Collider[] spawnColliders;
    private float spawnerTimer;
    private bool working = false;

    public void Start()
    {

    }

    public override void OnStartClient()
    {
        spawnColliders = GetComponents<Collider>();
        foreach (GameObject obj in npcPrefabs)
        {
            ClientScene.RegisterPrefab(obj);
        }
        StartCoroutine(StartSpawner());
        base.OnStartClient();
    }

    IEnumerator StartSpawner()
    {
        yield return new WaitForSeconds(0.5f);
        if (isServer)
        {
            working = true;
            for (int i = 0; i < initialNpcs; i++)
            {
                SpawnNpc();
            }
        }
    }

    [ServerCallback]
    void Update()
    {

        if (working && transform.childCount < maxNpcs)
        {
            spawnerTimer += Time.deltaTime;
            if (spawnerTimer > rateSecondsSpawn)
            {
                spawnerTimer -= rateSecondsSpawn;
                SpawnNpc();
            }
        }
    }

    public void SpawnNpc()
    {
        GameObject prefabToSpawn = npcPrefabs[UnityEngine.Random.Range(0, npcPrefabs.Length - 1)];
        Bounds whichSpawningZone = spawnColliders[UnityEngine.Random.Range(0, spawnColliders.Length - 1)].bounds;
        Vector3 positionNewNPC = new Vector3(UnityEngine.Random.Range(whichSpawningZone.min.x, whichSpawningZone.max.x),
            UnityEngine.Random.Range(whichSpawningZone.min.y, whichSpawningZone.max.y),
            UnityEngine.Random.Range(whichSpawningZone.min.z, whichSpawningZone.max.z));
        GameObject npc = Instantiate(prefabToSpawn, positionNewNPC, new Quaternion(0, 0, 0, 0), transform) as GameObject;
        NetworkServer.Spawn(npc);
    }
}
