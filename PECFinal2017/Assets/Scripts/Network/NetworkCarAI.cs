﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;
using System.Collections;

public class NetworkCarAI : NetworkBehaviour
{
    
    void Start()
    {
        Rigidbody _rigidbody = GetComponent<Rigidbody>();
        if (!isServer)
        {
            GetComponent<CarChildController>().enabled = false;
            GetComponent<UnityStandardAssets.Vehicles.Car.CarAIControl>().enabled = false;
            GetComponent<UnityStandardAssets.Utility.WaypointProgressTracker>().enabled = false;
            _rigidbody.useGravity = false;
            _rigidbody.detectCollisions = true;
        }
    }

    [Server]
    public void Explode()
    {
        NetworkServer.Destroy(gameObject);
    }
}

