﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;
using System.Collections;

[RequireComponent(typeof(NetworkTransform))]
public class NetworkNPC : NetworkBehaviour
{
    void Start()
    {
        Rigidbody _rigidbody = GetComponent<Rigidbody>();
        if (!isServer)
        {
            GetComponent<NavMeshAgent>().enabled = false;
            GetComponent<BehaviorExecutor>().enabled = false;
            GetComponent<Animator>().applyRootMotion = false;
            _rigidbody.useGravity = false;
            _rigidbody.detectCollisions = true;
        }
    }

    [Server]
    public void Explode()
    {
        NetworkServer.Destroy(gameObject);
    }
}
