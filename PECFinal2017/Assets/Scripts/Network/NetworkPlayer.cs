﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System;

[RequireComponent(typeof(NetworkTransform))]
[RequireComponent(typeof(Rigidbody))]
public class NetworkPlayer : NetworkBehaviour
{
    [SyncVar]
    public Color color;
    [SyncVar]
    public string playerName;

    public Health playerHealth;
    protected Rigidbody _rigidbody;

    protected float _rotation = 0;
    protected float _acceleration = 0;

    protected float _shootingTimer = 0;

    protected bool _canControl = true;

    //hard to control WHEN Init is called (networking make order between object spawning non deterministic)
    //so we call init from multiple location (depending on what between spaceship & manager is created first).
    protected bool _wasInit = false;

    void Awake()
    {
        //register the spaceship in the gamemanager, that will allow to loop on it.
        NetworkGameManager.sInstance.players.Add(this);
        playerHealth = GetComponent<Health>();
    }

    public override void OnStartClient()
    {
        Renderer[] rends = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in rends)
            r.material.color = color;
    }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        if (!isLocalPlayer)
        {
            GetComponent<Animator>().applyRootMotion = false;
            _rigidbody.useGravity = false;
            _rigidbody.detectCollisions = false;
        }

        if (NetworkGameManager.sInstance != null)
        {//we MAY be awake late (see comment on _wasInit above), so if the instance is already there we init
            Init();
        }
    }

    public void Init()
    {
        if (_wasInit)
            return;
        _wasInit = true;
    }

    void OnDestroy()
    {
        NetworkGameManager.sInstance.players.Remove(this);
    }

    public void ShowGameOver()
    {
        GameObject.Find("GameOver").GetComponent<Canvas>().enabled=true;
        GameObject.Find("GameOver").GetComponent<GraphicRaycaster>().enabled=true;
    }

    public void Spawn(Vector3 pos, Quaternion rotation)
    {
        if (hasAuthority)
        {
            _rigidbody.position = pos;
            _rigidbody.rotation = rotation;
        }
    }
}
