﻿using UnityEngine;
using UnityEngine.Networking;
using Prototype.NetworkLobby;
using System.Collections;
using System.Collections.Generic;

public class NetworkGameManager : NetworkBehaviour
{
    static public NetworkGameManager sInstance = null;

    [HideInInspector] public List<NetworkPlayer> players = new List<NetworkPlayer>();
    public Transform[] m_SpawnPoint;

    [Header("Canvases")]
    public GameObject hud;
    public GameObject menu;

    [Space]
    protected bool _running = true;

    void Awake()
    {
        sInstance = this;
    }

    void Start()
    {
        for(int i = 0; i < players.Count; ++i)
        {
            players[i].Init();
        }
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    public override void OnStartServer()
    {
        RpcSpawnPlayers();
    }

    [ClientRpc]
    void RpcSpawnPlayers()
    {
        for (int i = 0; i < players.Count; i++)
        {
            players[i].Spawn(m_SpawnPoint[i].position, m_SpawnPoint[i].rotation);
        }
    }

    
    void Update()
    {
        if (!isServer) return;
        if (!_running || players.Count == 0)
            return;

        bool allDestroyed = true;
        for (int i = 0; i < players.Count; ++i)
        {
            allDestroyed = players[i].playerHealth.currentLife <= 0;
            if (!allDestroyed) break;
        }

        if(allDestroyed)
        {
            StartCoroutine(ShowGameOver());
            StartCoroutine(ReturnToLobby());
        }
    }

    IEnumerator ShowGameOver()
    {
        _running = false;
        yield return new WaitForSeconds(3.0f);
        RpcShowGameOver();
    }

    [ClientRpc]
    void RpcShowGameOver()
    {
        for (int i = 0; i < players.Count; i++)
        {
            players[i].ShowGameOver();
        }
    }

    IEnumerator ReturnToLobby()
    {
        _running = false;
        yield return new WaitForSeconds(6.0f);
        LobbyManager.s_Singleton.ServerReturnToLobby();
    }
}
