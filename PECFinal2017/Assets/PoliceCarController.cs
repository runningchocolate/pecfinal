﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class PoliceCarController : MonoBehaviour
{
    public Transform waypointTargetObject;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Car")
        {
            if (other.transform.parent.GetComponent<CarController>().enabled)
            {
                Debug.Log("following car");
                transform.GetComponent<CarAIControl>().SetTarget(other.transform);
            }
            if (other.GetComponent<CarController>().enabled)
            {
                Debug.Log("following car");
                transform.GetComponent<CarAIControl>().SetTarget(other.transform);
            }
                
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Car")
        {
            if (other.transform.parent.GetComponent<CarController>().enabled)
            {
                Debug.Log("unfollowing car");
                transform.GetComponent<CarAIControl>().SetTarget(waypointTargetObject);
            }
            if (other.GetComponent<CarController>().enabled )
            {
                Debug.Log("unfollowing car");
                transform.GetComponent<CarAIControl>().SetTarget(waypointTargetObject);
            }
                
        }
    }
}
